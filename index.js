function Pokemon(name, health, attack){

	this.name = name;
	this.health = health;
	this.atk = attack;

	// Methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);

		let damageResult = target.health - this.atk;

		target.health = damageResult;

		console.log(`${target.name} health is now reduced to ${target.health}`);

		if(target.health === 0){
			console.log(`${target.name} fainted`);
		}
	};
};

let bulbasaur = new Pokemon("bulbasaur", 40, 10);
let rattata = new Pokemon("rattata", 30, 10);

bulbasaur.tackle(rattata);
rattata.tackle(bulbasaur);
bulbasaur.tackle(rattata);
rattata.tackle(bulbasaur);
bulbasaur.tackle(rattata);
